# P.I.W.O. project

## Introduction
The P.I.W.O project is the open-source player for light shows performed of different kinds of buildings.  
Historically the project used to make the shows on student dormitories, but there are no limits.   
To make the show you need some extra components, but more about it in the project wiki.  

## Dependencies
Run this commands to downlaod required dependencies
```bash
git submodule update --init --recursive
./tools/install_dep
```

## Build
### Windows
TODO

### Linux
* Install necessary packages
```bash
sudo apt install qt5-default libqt5opengl5-dev
```


## Additional Links
[Launchpad protocol](https://docs.google.com/document/d/1KvSllQo9GsYYoJ09QXKexN2dBp_79TAcYuCTSapX41o)
[UDP protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)
[TCP protocol](https://docs.google.com/document/d/1qdNjDpIlg-bFNPkFcjgMsGNjFXdrhj7mQ_jBvjID3Zw)
[PIWO protocol](https://docs.google.com/document/d/1Id8atthziZ64V6xowBAZt2FVeHfraQnJGya-9A8t3F8)
